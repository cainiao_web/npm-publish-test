## bk-element

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### 发版命令

```

先登录
npm login

npm version major : 主版本号加 1，其余版本号归 0
npm version minor : 次版本号加 1，修订号归 0
npm version patch : 修订号加 1
npm version 版本号 ：设置版本号为指定的版本号
npm version prerelease : 先⾏版本号增加1
npm version prerelease --preid=alpha 假设现在的version是1.3.5，执⾏完该命令之后就会成为 1.3.6-alpha.0

npm version prerelease -m "update %s“  修改提交信息

npm publish 发布
npm unpublish 包名 ：在npm上删除了指定的包。
```

### 使用方式

目前组件包含 [BkBtn, Transfer, SearchForm, BkTable, EditForm]
文档在doc文件夹下
组件源码在plugins文件夹下

```
import BkBtn from 'bk-element'
import 'bk-element/dist/bk-element.css'

vue.use(BkBtn)


```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
