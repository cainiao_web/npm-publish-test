import Vue from 'vue'
import App from './App.vue'
// import '@/styles/index.scss' // global css
// import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css' 
// import 'prismjs/themes/prism.css';
// import './pages/transfer'
// window.Vue = Vue
import componentsGroups from '../plugins/index'
// import "llgtfoo-components-box/dist/llgtfoo-components-box.css";

Vue.use(componentsGroups)
// Vue.use(ElementUI, {size: 'small', zIndex: 3000})

new Vue({
  render: h => h(App),
}).$mount('#app')
