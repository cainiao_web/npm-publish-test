import Vue from 'vue'
import Store from '@/store'

Vue.directive('permission', {
    inserted(el, binding, vnode) {
        const {
            value
        } = binding;
        let permissionList = Store.getters.permissionList;
        if (value) {
            const permissionRoles = value;

            const hasPermission = permissionList.some(role => {
                return permissionRoles === role;
            })
            if (!hasPermission) {
                el.parentNode && el.parentNode.removeChild(el);
            }
        } else {
            return true
        }
    }
})