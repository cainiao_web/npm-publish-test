import Vue from 'vue'

Vue.directive('reset', {
    // bind: function(el) {
    //     // 聚焦元素
    //     // let nodes = document.getElementById(el.id)
    // },
    // inserted: function(el, bind) {
    //     // console.log(el, 'insert')
    // },
    // update: function(el, bind) {
    //     console.log(el, el.value, 'update')
    // },
    // componentUpdated(el) {
    //     console.log(el, el.value, 'componentUpdated')
    // },
    unbind(el, bind) {
        let form = bind.value
        if(form.type == 'array') {
            form.ruleForm[form.code] = []
        } else if(form.type == 'tableArray'){
            form.ruleForm[form.code].forEach((e) => {
                Object.keys(e).forEach(ele => {
                    e[ele] = ''
                })
            })
        } else {
            form.ruleForm[form.code] = ''
        }
        
    }
})