// // import Vue from 'vue'
// import Transfer from './components/transfer'

// Transfer.install = Vue => Vue.component(Transfer.name, Transfer)

// export default Transfer

//组件
// import Demo from "./components/demo"

import BkBtn from "./components/bkBtn"
import Transfer from "./components/transfer"
import SearchForm from "./components/searchForm/searchForm"
import BkTable from "./components/table/bkTable"
import "./components/table/index.jsx";
import EditForm from "./components/editForm/editForm"

import  './components/vdirective'

//指令
// import 


// Vue.component('Transfer', Transfer)
// const directives = []
// const filters = []
// // import DateTime from "./components/dateTime/date-time.vue";
// // import selectDate from "./components/selectDate/index.vue";
// // import numberScroll from "./components/number-scroll/index.vue";
// // import tabPage from "./components/tabPage/index.vue";
// // import listScroll from "./components/list-scroll/index.vue";

// // //指令
// // // import * as directives from "./directives/*/index.js";
// // import autoScale from './directives/auto-scale/index.js'
// // import waterMarker from './directives/water-marker/index.js'

// // //过滤器
// // import numberFormat from './Filters/number-format/index.js'

// // //所有组件列表
// // const components = [DateTime, selectDate, numberScroll, tabPage, listScroll];
const components = [BkBtn, Transfer, SearchForm, BkTable, EditForm];
// // //所以指令
// const directives = [throttle];
// import * as directives from "./components/vdirective"
// // z
// // const filters = [numberFormat]

//定义install方法，Vue作为参数
const install = Vue => {
  //判断是否安装，安装过就不用继续执行
  if (install.installed) return;
  install.installed = true;
  //遍历注册所有组件
  components.map(component => {
    Vue.component(component.name, component)
  });

  // // //遍历注册所有指令
  // directives.map(directive => {
  //   console.log(directive)
  //   Vue.use(directive)
  // });

  // //遍历过滤器
  // filters.map(filters => Vue.use(filters));
};

//检测到Vue再执行
if (typeof window !== "undefined" && window.Vue) {
  install(window.Vue);
}

export default {
  install,
  //所有组件，必须具有install方法才能使用Vue.use()
  ...components
};